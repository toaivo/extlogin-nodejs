#!/bin/bash
#  pull the latest code from the S3 bucket
cd $HOME/build.js
aws s3 cp $S3BUCKET/extlogin.tar.gz .
rm -rf app/*
tar -xzvf extlogin.tar.gz 
docker build -t extlogin -f Dockerfile.js .
