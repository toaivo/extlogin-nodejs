FROM node:14.11.0-buster
WORKDIR /usr/src/app
COPY app/ /usr/src/app/
RUN npm install

EXPOSE 3000

CMD ["node", "bin/www"]
